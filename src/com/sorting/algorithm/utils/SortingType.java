package com.sorting.algorithm.utils;

public enum SortingType {
	ASCENDING,
	DESCENDING
}
