package com.sorting.algorithm.utils;

import java.util.Random;

public class Shuffle {

    /**
     * Fisher�Yates shuffle.
     */
    public static int[] shuffle(int[] array) {
        Random random = new Random();
        for (int i = array.length - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);
            int a = array[index];
            array[index] = array[i];
            array[i] = a;
        }
        
        return array;
    }
}