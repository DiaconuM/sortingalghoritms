package com.sorting.algorithm.impl;

import com.sorting.algorithm.utils.SortingType;

/**
 * Merge sort is comparison-based sorting algorithm. 
 * Merge sort is a stable sort, which means that the implementation preserves the input order of equal elements in the sorted output.
 */
public class MergeSort {
    private int[] helper;

    private int number;

    public MergeSort() {

    }

    public int[] sort(int[] vector, SortingType sortingType) {
        if (sortingType == null) {
            sortingType = SortingType.ASCENDING;
        }
        
        this.number = vector.length;
        this.helper = new int[number];
        mergeSort(vector, 0, number - 1, sortingType);
        
        return vector;
    }

    private void mergeSort(int[] vector, int low, int high, SortingType sortingType) {
        // check if low is smaller then high, if not then the array is sorted
        if (low < high) {
            // Get the index of the element which is in the middle
            int middle = low + (high - low) / 2;
            // Sort the left side of the array
            mergeSort(vector, low, middle, sortingType);
            // Sort the right side of the array
            mergeSort(vector, middle + 1, high, sortingType);
            // Combine them both
            merge(vector, low, middle, high, sortingType);
        }
    }

    private void merge(int[] vector, int low, int middle, int high, SortingType sortingType) {
        // Copy both parts into the helper array
        for (int i = low; i <= high; i++) {
            helper[i] = vector[i];
        }

        int i = low;
        int j = middle + 1;
        int k = low;
        // Copy the smallest values from either the left or the right side back
        // to the original array
        while (i <= middle && j <= high) {
            if (SortingType.ASCENDING.equals(sortingType)) { 
                if (helper[i] <= helper[j]) {
                    vector[k] = helper[i];
                    i++;
                } else {
                    vector[k] = helper[j];
                    j++;
                }
            }
            
            if (SortingType.DESCENDING.equals(sortingType)) { 
                if (helper[i] >= helper[j]) {
                    vector[k] = helper[i];
                    i++;
                } else {
                    vector[k] = helper[j];
                    j++;
                }
            }
            
            k++;
        }
        // Copy the rest of the left side of the array into the target array
        while (i <= middle) {
            vector[k] = helper[i];
            k++;
            i++;
        }
    }
}
