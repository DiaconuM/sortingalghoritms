package com.sorting.algorithm.impl;

import com.sorting.algorithm.utils.SortingType;

/**
 * Heapsort is a comparison-based sorting algorithm to create a sorted array (or list), and is part of the selection sort family. 
 * Although somewhat slower in practice on most machines than a well-implemented quicksort, 
 * it has the advantage of a more favorable worst-case O(n log n) runtime.
 */
public class HeapSort {

    private int n;

    private int left;

    private int right;

    private int largest;

    public HeapSort() {

    }

    private void buildheap(int[] vector, SortingType sortingType) {
        n = vector.length - 1;
        for (int i = n / 2; i >= 0; i--) {
            maxheap(vector, i, sortingType);
        }
    }

    private void maxheap(int[] vector, int i, SortingType sortingType) {
        left = 2 * i;
        right = 2 * i + 1;

        sortingByType(vector, i, sortingType);

        if (largest != i) {
            exchange(vector, i, largest);
            maxheap(vector, largest, sortingType);
        }
    }

    private void sortingByType(int[] vector, int i, SortingType sortingType) {
        if (SortingType.ASCENDING.equals(sortingType)) {
            if (left <= n && vector[left] > vector[i]) {
                largest = left;
            } else {
                largest = i;
            }

            if (right <= n && vector[right] > vector[largest]) {
                largest = right;
            }
        }

        if (SortingType.DESCENDING.equals(sortingType)) {
            if (left <= n && vector[left] < vector[i]) {
                largest = left;
            } else {
                largest = i;
            }

            if (right <= n && vector[right] < vector[largest]) {
                largest = right;
            }
        }
    }

    private void exchange(int[] vector, int i, int j) {
        int temp = vector[i];
        vector[i] = vector[j];
        vector[j] = temp;
    }

    public int[] sort(int[] vector, SortingType sortingType) {
        if (sortingType == null) {
            sortingType = SortingType.ASCENDING;
        }

        buildheap(vector, sortingType);
        for (int i = n; i > 0; i--) {
            exchange(vector, 0, i);
            n = n - 1;
            maxheap(vector, 0, sortingType);
        }
        
        return vector;
    }
}