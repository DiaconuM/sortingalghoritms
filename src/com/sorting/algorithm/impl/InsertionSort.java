package com.sorting.algorithm.impl;

import com.sorting.algorithm.utils.SortingType;

/**
 * Insertion sort is a simple sorting algorithm that builds the final sorted array one item at a time. 
 * It is much less efficient on large lists than more advanced algorithms such as quicksort, heapsort, or merge sort.
 */
public class InsertionSort {

    public InsertionSort() {
        
    }

    public int[] sort(int[] vector, SortingType sortingType) {
        if (sortingType == null) {
            sortingType = SortingType.ASCENDING;
        }

        for (int i = 1; i < vector.length; i++) {
            int temp = vector[i];
            int j = i;

            j = sortingByType(vector, sortingType, temp, j);

            vector[j] = temp;
        }
        
        return vector;
    }

    private int sortingByType(int[] vector, SortingType sortingType, int temp, int j) {
        if (SortingType.ASCENDING.equals(sortingType)) {
            while (j > 0 && vector[j - 1] > temp) {
                vector[j] = vector[j - 1];
                j--;
            }
        }

        if (SortingType.DESCENDING.equals(sortingType)) {
            while (j > 0 && vector[j - 1] < temp) {
                vector[j] = vector[j - 1];
                j--;
            }
        }
        
        return j;
    }
}
