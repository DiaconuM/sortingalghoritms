package com.sorting.algorithm.impl;

import com.sorting.algorithm.utils.SortingType;

/**
 * Bubble Sort is a simple sorting algorithm that works by repeatedly stepping through the list to be sorted, 
 * comparing each pair of adjacent items and swapping them if they are in the wrong order.
 */
public class BubbleSort {

    public BubbleSort() {
        
    }

    public int[] sort(int[] vector, SortingType sortingType) {
        if (vector == null || vector.length == 0) {
            System.out.println("There are no items to sort.");
            return null;
        }
        if (sortingType == null) {
            sortingType = SortingType.ASCENDING;
        }
        
        for (int i = 0; i < vector.length; i++) {
            for (int j = 0; j < (vector.length - 1) - i; j++) {
                sortingByType(vector, j, sortingType);
            }
        }
        
        return vector;
    }

    private void sortingByType(int[] vector, int j, SortingType sortingType) {
        int temp;
        if (SortingType.ASCENDING.equals(sortingType)) {
            if (vector[j] > vector[j + 1]) {
                temp = vector[j];
                vector[j] = vector[j + 1];
                vector[j + 1] = temp;
            }
        }

        if (SortingType.DESCENDING.equals(sortingType)) {
            if (vector[j + 1] > vector[j]) {
                temp = vector[j + 1];
                vector[j + 1] = vector[j];
                vector[j] = temp;
            }
        }
        
    }
}