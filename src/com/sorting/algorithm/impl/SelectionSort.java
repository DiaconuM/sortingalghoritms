package com.sorting.algorithm.impl;

import com.sorting.algorithm.utils.SortingType;

public class SelectionSort {

    public SelectionSort() {

    }

    public int[] sort(int[] vector, SortingType sortingType) {
        if (sortingType == null) {
            sortingType = SortingType.ASCENDING;
        }

        for (int i = 0; i < vector.length - 1; i++) {
            int index = i;
            for (int j = i + 1; j < vector.length; j++) {
                index = sortingByType(vector, sortingType, index, j);
            }

            int smallerNumber = vector[index];
            vector[index] = vector[i];
            vector[i] = smallerNumber;
        }
        
        return vector;
    }

    private int sortingByType(int[] vector, SortingType sortingType, int index, int j) {
        if (SortingType.ASCENDING.equals(sortingType)) {
            if (vector[j] < vector[index]) {
                index = j;
            }
        }

        if (SortingType.DESCENDING.equals(sortingType)) {
            if (vector[j] > vector[index]) {
                index = j;
            }
        }
        return index;
    }
}
