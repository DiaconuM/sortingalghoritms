package com.sorting.algorithm.impl;

import com.sorting.algorithm.utils.SortingType;

/**
 * Quicksort is a simple sorting algorithm using the divide-and-conquer recursive procedure. 
 * It is the quickest comparison-based sorting algorithm in practice with an average running time of O(n log(n)).It is also known as partition-exchange sort.
 */
public class QuickSort {

    public QuickSort() {

    }

    public int[] sort(int[] vector, int left, int right, SortingType sortingType) {
        if (left >= right) {
            return null;
        }
        
        if (sortingType == null) {
            sortingType = SortingType.ASCENDING;
        }

        // pick the pivot
        int middle = left + (right - left) / 2;
        double pivot = vector[middle];

        // make left < pivot and right > pivot
        int i = left, j = right;
        while (i <= j) {
            if (SortingType.ASCENDING.equals(sortingType)) {
                while (vector[i] < pivot) {
                    i++;
                }
    
                while (vector[j] > pivot) {
                    j--;
                }
            }
            
            if (SortingType.DESCENDING.equals(sortingType)) {
                while (vector[i] > pivot) {
                    i++;
                }
    
                while (vector[j] < pivot) {
                    j--;
                }
            }
            
            if (i <= j) {
                int temp = vector[i];
                vector[i] = vector[j];
                vector[j] = temp;
                i++;
                j--;
            }
        }

        // recursively sort two sub parts
        if (left < j) {
            sort(vector, left, j, sortingType);
        }

        if (right > i) {
            sort(vector, i, right, sortingType);
        }
        
        return vector;
    }
}