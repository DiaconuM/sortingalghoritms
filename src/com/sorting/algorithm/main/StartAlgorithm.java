package com.sorting.algorithm.main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;

import com.sorting.algorithm.impl.BubbleSort;
import com.sorting.algorithm.impl.HeapSort;
import com.sorting.algorithm.impl.InsertionSort;
import com.sorting.algorithm.impl.MergeSort;
import com.sorting.algorithm.impl.QuickSort;
import com.sorting.algorithm.impl.SelectionSort;
import com.sorting.algorithm.utils.Shuffle;
import com.sorting.algorithm.utils.SortingType;

public class StartAlgorithm {

	private static final String OUT_EXTENSION = ".out";
	private static final String IN_EXTENSION = ".in";
	
	private static final String RESULTS_FOLDER = "results";
	private static final String INPUT_FOLDER = "input";
	
    private static int LENGTH = 10000;
    
    private static int[] ascendingVector;
    private static int[] descendingVector;
    private static int[] randomVector;
    
	private static void writeToFile(String folder, String filename, String text) {
	    String outputFolder = null; 
	    String extension = null;
	    if (INPUT_FOLDER.equals(folder)) {
	        outputFolder = folder;
	        extension = IN_EXTENSION;
	    } else {
	        outputFolder = RESULTS_FOLDER + "/" + folder;
	        extension = OUT_EXTENSION;
	    }
	    
	    File directory = new File(outputFolder);
	    if (!directory.exists()) {
            directory.mkdirs();
        }
	    
		String pathName = outputFolder + "/" + filename + extension; 
		try (Writer writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(pathName), "utf-8"))) {
			writer.write(text);
		} catch (Exception e) {
			System.err.println("ERROR: " + e.getMessage());
		}
	}

    public static void main(String[] args) throws IOException {
        
        initializeInputFiles();
        
        System.out.println("************************ START BUBBLE SORT ALGORITHM ************************");
        startBubbleSortAlgorithm();

        System.out.println();

        System.out.println("************************ START QUICK SORT ALGORITHM ************************");
        startQuickSortAlgorithm();
        System.out.println();

        System.out.println("************************ START INSERTION SORT ALGORITHM ************************");
        startInsertionSortAlgorithm();

        System.out.println();
        
        System.out.println("************************ START HEAP SORT ALGORITHM ************************");
        startHeapSortAlgorithm();

        System.out.println();
        
        System.out.println("************************ START MERGE SORT ALGORITHM ************************");
        startMergeSortAlgorithm();

        System.out.println();
        
        System.out.println("************************ START SELECTION SORT ALGORITHM ************************");
        startSelectionSortAlgorithm();
        
    }

    private static void initializeInputFiles() {
        int[] createdAscendingInputElements = createInputElements(SortingType.ASCENDING, LENGTH);
        writeToFile(INPUT_FOLDER, "ascending", Arrays.toString(createdAscendingInputElements));
        
        int[] createdDescendingInputElements = createInputElements(SortingType.DESCENDING, LENGTH);
        writeToFile(INPUT_FOLDER, "descending", Arrays.toString(createdDescendingInputElements));
        
        int[] createdRandomInputElements = createInputElements(null, LENGTH);
        writeToFile(INPUT_FOLDER, "random", Arrays.toString(createdRandomInputElements));
    }
    
    private static int[] createInputElements(SortingType sortingType, int length) {
        int[] vector = new int[length];

        if (sortingType == null) {
            //create random elements
            for (int i = 1; i <= vector.length; i++) {
                vector[i - 1] = i;
            }
            vector = Shuffle.shuffle(vector);
        }
        
        if (SortingType.ASCENDING.equals(sortingType)) {
            //create ascending elements
            for (int i = 1; i <= vector.length; i++) {
                vector[i - 1] = i;
            }
        }
        
        if (SortingType.DESCENDING.equals(sortingType)) {
            //create descending elements
            for (int i = vector.length; i >= 1; i--) {
                vector[vector.length - i] = i;
            }
        }
        
        return vector;
    }

    private static int[] readFile(String filename) throws IOException {
    	int[] vector = null;
    	
    	File file = null;
    	FileInputStream fis = null;
    	String path = INPUT_FOLDER + "/" + filename + IN_EXTENSION;
    	try {
    		file = new File(path);
    		byte[] bytes = new byte[(int) file.length()];
    		fis = new FileInputStream(file);
    		fis.read(bytes);
    		
    		String bytesAsString = new String(bytes);
    		
    		String[] valueStr = bytesAsString.substring(1, bytesAsString.length() - 1).trim().split(", ");
    		vector = new int[valueStr.length];
    		
    		for (int i = 0; i < valueStr.length; i++) {
    			vector[i] = Integer.parseInt(valueStr[i]);
    		}
    		
    	} catch(IOException e) {
    		if (fis != null) {
    			fis.close();
    		}
    	}
    	
    	return vector;
    }
    
    private static void initializeVectors() throws IOException {
        ascendingVector = readFile("ascending");
        descendingVector = readFile("descending");
        randomVector = readFile("random");
    }
    
    private static void startBubbleSortAlgorithm() throws IOException {
    	initializeVectors();
    	
    	double startSorting;
    	double endSorting;
    	
        BubbleSort bubbleSort = new BubbleSort();
        
        startSorting = System.currentTimeMillis();
        int[] sortedAscendingVector = bubbleSort.sort(ascendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("bubble ascending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("bubbleSort", "bubble_sort_ascending", Arrays.toString(sortedAscendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedDescendingVector = bubbleSort.sort(descendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("bubble descending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("bubbleSort", "bubble_sort_descending", Arrays.toString(sortedDescendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedRandomVector = bubbleSort.sort(randomVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("bubble random: ");
        System.out.println(endSorting-startSorting);
        writeToFile("bubbleSort", "bubble_sort_random", Arrays.toString(sortedRandomVector));
    }

    private static void startQuickSortAlgorithm() throws IOException {
        initializeVectors();
        
        double startSorting;
        double endSorting;
        
        int left = 0;
        int right = LENGTH - 1;
        
        QuickSort quickSort = new QuickSort();
        
        startSorting = System.currentTimeMillis();
        int[] sortedAscendingVector = quickSort.sort(ascendingVector, left, right, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("quickSort ascending: ");
        System.out.println(endSorting-startSorting);        
        writeToFile("quickSort", "quick_sort_ascending", Arrays.toString(sortedAscendingVector));
        
        right = descendingVector.length - 1;
        startSorting = System.currentTimeMillis();
        int[] sortedDescendingVector =  quickSort.sort(descendingVector, left, right, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("quickSort descending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("quickSort", "quick_sort_descending", Arrays.toString(sortedDescendingVector));
        
        right = randomVector.length - 1;
        startSorting = System.currentTimeMillis();
        int[] sortedRandomVector = quickSort.sort(randomVector, left, right, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("quickSort random: ");
        System.out.println(endSorting-startSorting);
        writeToFile("quickSort", "quick_sort_random", Arrays.toString(sortedRandomVector));
    }

    private static void startInsertionSortAlgorithm() throws IOException {
        initializeVectors();
        
        double startSorting;
        double endSorting;
        
        InsertionSort insertionSort = new InsertionSort();
        
        startSorting = System.currentTimeMillis();
        int[] sortedAscendingVector = insertionSort.sort(ascendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("insertionSort ascending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("insertionSort", "insertion_sort_ascending", Arrays.toString(sortedAscendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedDescendingVector = insertionSort.sort(descendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("insertionSort descending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("insertionSort", "insertion_sort_descending", Arrays.toString(sortedDescendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedRandomVector = insertionSort.sort(randomVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("insertionSort random: ");
        System.out.println(endSorting-startSorting);
        writeToFile("insertionSort", "insertion_sort_random", Arrays.toString(sortedRandomVector));
    }

    private static void startHeapSortAlgorithm() throws IOException {
        initializeVectors();
        
        double startSorting;
        double endSorting;
        
        HeapSort heapSort = new HeapSort();
        
        startSorting = System.currentTimeMillis();
        int[] sortedAscendingVector = heapSort.sort(ascendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("heapSort ascending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("heapSort", "heap_sort_ascending", Arrays.toString(sortedAscendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedDescendingVector = heapSort.sort(descendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("heapSort descending: ");
        System.out.println(endSorting-startSorting);        
        writeToFile("heapSort", "heap_sort_descending", Arrays.toString(sortedDescendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedRandomVector = heapSort.sort(randomVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("heapSort random: ");
        System.out.println(endSorting-startSorting);
        writeToFile("heapSort", "heap_sort_random", Arrays.toString(sortedRandomVector));
    }

    private static void startMergeSortAlgorithm() throws IOException {
    	initializeVectors();
        
    	double startSorting;
        double endSorting;
    	
        MergeSort mergeSort = new MergeSort();
        
        startSorting = System.currentTimeMillis();
        int[] sortedAscendingVector = mergeSort.sort(ascendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("mergeSort ascending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("mergeSort", "merge_sort_ascending", Arrays.toString(sortedAscendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedDescendingVector = mergeSort.sort(descendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("mergeSort descending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("mergeSort", "merge_sort_descending", Arrays.toString(sortedDescendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedRandomVector = mergeSort.sort(randomVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("mergeSort random: ");
        System.out.println(endSorting-startSorting);
        writeToFile("mergeSort", "merge_sort_random", Arrays.toString(sortedRandomVector));
    }

    private static void startSelectionSortAlgorithm() throws IOException {
        initializeVectors();
        
        double startSorting;
        double endSorting;
        
        SelectionSort selectionSort = new SelectionSort();

        startSorting = System.currentTimeMillis();
        int[] sortedAscendingVector = selectionSort.sort(ascendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("selectionSort ascending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("selectionSort", "selection_sort_ascending", Arrays.toString(sortedAscendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedDescendingVector = selectionSort.sort(descendingVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("selectionSort descending: ");
        System.out.println(endSorting-startSorting);
        writeToFile("selectionSort", "selection_sort_descending", Arrays.toString(sortedDescendingVector));
        
        startSorting = System.currentTimeMillis();
        int[] sortedRandomVector = selectionSort.sort(randomVector, SortingType.ASCENDING);
        endSorting = System.currentTimeMillis();
        System.out.print("selectionSort random: ");
        System.out.println(endSorting-startSorting);
        writeToFile("selectionSort", "selection_sort_random", Arrays.toString(sortedRandomVector));
    }
}
