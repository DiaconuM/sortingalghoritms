package com.sorting.algorithm.start;

import com.sorting.algorithm.impl.InsertionSort;
import com.sorting.algorithm.utils.SortingType;

public class StartInsertionSort {
    private InsertionSort insertionSort;
//    private Shuffle shuffle;

    public StartInsertionSort() {
        this.insertionSort = new InsertionSort();
//        this.shuffle = new Shuffle();
    }
    
    public int[] sort(int[] vector) {
        return insertionSort.sort(vector, SortingType.ASCENDING);
    }

//    public StringBuilder insertionSortRandom(int[] vector) {
//        StringBuilder builder = new StringBuilder();
//
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] randomVector = shuffle.shuffle(vector);
//        endSorting = System.currentTimeMillis();
//        
//        builder.append(Arrays.toString(randomVector));
//
//        System.out.print("insertion random: ");
//        System.out.println(endSorting-startSorting);
//
//        return builder;
//    }
//
//    public StringBuilder insertionSortAscending(int[] vectorToBeSortedAscending) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] ascendingSortedVector = insertionSort.sort(vectorToBeSortedAscending, SortingType.ASCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(ascendingSortedVector));
//
//        System.out.print("insertion ascending: ");
//        System.out.println(endSorting-startSorting);
//        
//        return builder;
//    }
//
//    public StringBuilder insertionSortDescending(int[] vectorToBeSortedDescending) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] descendingSortedVector = insertionSort.sort(vectorToBeSortedDescending, SortingType.DESCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(descendingSortedVector));
//
//        System.out.print("insertion descending: ");
//        System.out.println(endSorting-startSorting);
//        
//        return builder;
//    }
}
