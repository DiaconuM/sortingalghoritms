package com.sorting.algorithm.start;

import com.sorting.algorithm.impl.HeapSort;
import com.sorting.algorithm.utils.SortingType;

public class StartHeapSort {

    private HeapSort heapSort;
//    private Shuffle shuffle;

    public StartHeapSort() {
        this.heapSort = new HeapSort();
//        this.shuffle = new Shuffle();
    }
    
    public int[] sort(int[] vector) {
        return heapSort.sort(vector, SortingType.ASCENDING);
    }

//    public StringBuilder heapSortRandom(int[] vector) {
//        StringBuilder builder = new StringBuilder();
//
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] randomVector = shuffle.shuffle(vector);
//        endSorting = System.currentTimeMillis();
//        
//        builder.append(Arrays.toString(randomVector));
//
//        System.out.print("heap random: ");
//        System.out.println(endSorting-startSorting);
//
//        return builder;
//    }
//
//    public StringBuilder heapSortAscending(int[] vectorToBeSortedAscending) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] ascendingSortedVector = heapSort.sort(vectorToBeSortedAscending, SortingType.ASCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(ascendingSortedVector));
//
//        System.out.print("heap ascending: ");
//        System.out.println(endSorting-startSorting);
//        
//        
//        return builder;
//    }
//
//    public StringBuilder heapSortDescending(int[] vectorToBeSortedDescending) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] descendingSortedVector = heapSort.sort(vectorToBeSortedDescending, SortingType.DESCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(descendingSortedVector));
//
//        System.out.print("heap descending: ");
//        System.out.println(endSorting-startSorting);
//        
//        
//        return builder;
//    }
}
