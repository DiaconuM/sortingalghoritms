package com.sorting.algorithm.start;

import com.sorting.algorithm.impl.MergeSort;
import com.sorting.algorithm.utils.SortingType;

public class StartMergeSort {
    private MergeSort mergeSort;
//    private Shuffle shuffle;

    public StartMergeSort() {
        this.mergeSort = new MergeSort();
//        this.shuffle = new Shuffle();
    }
    
    public int[] sort(int[] vector) {
        return mergeSort.sort(vector, SortingType.ASCENDING);
    }

//    public StringBuilder mergeSortRandom(int[] vector) {
//        StringBuilder builder = new StringBuilder();
//
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] randomVector = shuffle.shuffle(vector);
//        endSorting = System.currentTimeMillis();
//        
//        builder.append(Arrays.toString(randomVector));
//
//        System.out.print("merge random: ");
//        System.out.println(endSorting-startSorting);
//
//        return builder;
//    }
//
//    public StringBuilder mergeSortAscending(int[] vectorToBeSortedAscending) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] ascendingSortedVector = mergeSort.sort(vectorToBeSortedAscending, SortingType.ASCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(ascendingSortedVector));
//
//        System.out.print("merge ascending: ");
//        System.out.println(endSorting-startSorting);
//        
//        return builder;
//    }
//
//    public StringBuilder mergeSortDescending(int[] vectorToBeSortedDescending) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] descendingSortedVector = mergeSort.sort(vectorToBeSortedDescending, SortingType.DESCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(descendingSortedVector));
//
//        System.out.print("merge descending: ");
//        System.out.println(endSorting-startSorting);
//        
//        return builder;
//    }
}
