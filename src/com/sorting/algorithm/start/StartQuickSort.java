package com.sorting.algorithm.start;

import com.sorting.algorithm.impl.QuickSort;
import com.sorting.algorithm.utils.SortingType;

public class StartQuickSort {
    private QuickSort quickSort;
//    private Shuffle shuffle;
    
    public StartQuickSort() {
        this.quickSort = new QuickSort();
//        this.shuffle = new Shuffle();
    }

    public int[] sort(int[] vector, int left, int right) {
        return quickSort.sort(vector, left, right, SortingType.ASCENDING);
    }
    
//    public StringBuilder quickSortRandom(int[] vector, int left, int right) {
//        StringBuilder builder = new StringBuilder();
//
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] randomVector = shuffle.shuffle(vector);
//        endSorting = System.currentTimeMillis();
//        
//        builder.append(Arrays.toString(randomVector));
//
//        System.out.print("quick random: ");
//        System.out.println(endSorting-startSorting);
//
//        return builder;
//    }
//
//    public StringBuilder quickSortAscending(int[] vectorToBeSortedAscending, int left, int right) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] ascendingSortedVector = quickSort.sort(vectorToBeSortedAscending, left, right, SortingType.ASCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(ascendingSortedVector));
//
//        System.out.print("quick ascending: ");
//        System.out.println(endSorting-startSorting);
//        
//        return builder;
//    }
//
//    public StringBuilder quickSortDescending(int[] vectorToBeSortedDescending, int left, int right) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] descendingSortedVector = quickSort.sort(vectorToBeSortedDescending, left, right, SortingType.DESCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(descendingSortedVector));
//
//        System.out.print("quick descending: ");
//        System.out.println(endSorting-startSorting);
//        
//        return builder;
//    }
}
