package com.sorting.algorithm.start;

import com.sorting.algorithm.impl.SelectionSort;
import com.sorting.algorithm.utils.SortingType;

public class StartSelectionSort {
    private SelectionSort selectionSort;
//    private Shuffle shuffle;

    public StartSelectionSort() {
        this.selectionSort = new SelectionSort();
//        this.shuffle = new Shuffle();
    }

    public int[] sort(int[] vector) {
        return selectionSort.sort(vector, SortingType.ASCENDING);
    }
    
//    public StringBuilder selectionSortRandom(int[] vector) {
//        StringBuilder builder = new StringBuilder();
//
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] randomVector = shuffle.shuffle(vector);
//        endSorting = System.currentTimeMillis();
//        
//        builder.append(Arrays.toString(randomVector));
//
//        System.out.print("selection random: ");
//        System.out.println(endSorting-startSorting);
//
//        return builder;
//    }
//
//    public StringBuilder selectionSortAscending(int[] vectorToBeSortedAscending) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] ascendingSortedVector = selectionSort.sort(vectorToBeSortedAscending, SortingType.ASCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(ascendingSortedVector));
//
//        System.out.print("selection ascending: ");
//        System.out.println(endSorting-startSorting);
//        
//        return builder;
//    }
//
//    public StringBuilder selectionSortDescending(int[] vectorToBeSortedDescending) {
//        StringBuilder builder = new StringBuilder();
//        long startSorting;
//        long endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] descendingSortedVector = selectionSort.sort(vectorToBeSortedDescending, SortingType.DESCENDING);
//        endSorting = System.currentTimeMillis();
//
//        builder.append(Arrays.toString(descendingSortedVector));
//
//        System.out.print("selection descending: ");
//        System.out.println(endSorting-startSorting);
//        
//        return builder;
//    }
}
