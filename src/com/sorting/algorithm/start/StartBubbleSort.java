package com.sorting.algorithm.start;

import com.sorting.algorithm.impl.BubbleSort;
import com.sorting.algorithm.utils.SortingType;

public class StartBubbleSort {

    private BubbleSort bubbleSort;
//    private Shuffle shuffle;

    public StartBubbleSort() {
        this.bubbleSort = new BubbleSort();
//        this.shuffle = new Shuffle();
    }
    
    public int[] sort(int[] vector) {
        return bubbleSort.sort(vector, SortingType.ASCENDING);
    }

//    public StringBuilder bubbleSortRandom(int[] vector) {
//        StringBuilder builder = new StringBuilder();
//
//        double startSorting;
//        double endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] randomVector = shuffle.shuffle(vector);
//        endSorting = System.currentTimeMillis();
//        System.out.print("bubble random: ");
//        System.out.println(endSorting-startSorting);
//
//        builder.append(Arrays.toString(randomVector));
//
//        return builder;
//    }
//
//    public StringBuilder bubbleSortAscending(int[] vectorToBeSortedAscending) {
//        StringBuilder builder = new StringBuilder();
//        
//        double startSorting;
//        double endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] ascendingSortedVector = bubbleSort.sort(vectorToBeSortedAscending, SortingType.ASCENDING);
//        endSorting = System.currentTimeMillis();
//        System.out.print("bubble ascending: ");
//        System.out.println(endSorting-startSorting);
//        
//        builder.append(Arrays.toString(ascendingSortedVector));
//        
//        return builder;
//    }
//
//    public StringBuilder bubbleSortDescending(int[] vectorToBeSortedDescending) {
//        StringBuilder builder = new StringBuilder();
//        
//        double startSorting;
//        double endSorting;
//
//        startSorting = System.currentTimeMillis();
//        int[] descendingSortedVector = bubbleSort.sort(vectorToBeSortedDescending, SortingType.DESCENDING);
//        endSorting = System.currentTimeMillis();
//        System.out.print("bubble descending: ");
//        System.out.println(endSorting-startSorting);
//        
//        builder.append(Arrays.toString(descendingSortedVector));
//        
//        return builder;
//    }
}
